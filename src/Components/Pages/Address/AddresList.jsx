import React from 'react'
import './Address.css'
export default function AddresList({useraddress,handleDeleteAddres,handleEditAddres}) {
    const addressList = useraddress&&useraddress.length > 0 ? useraddress.map(addres=>{
        return <>
        <li className="list-group-item custom-list" key={addres.id} id={addres.id}><div>{addres.name}</div> <div key={addres.id} className="action-btn"><button  onClick={handleDeleteAddres} className="btn btn-danger" id={addres.id}>حذف</button> <button id={addres.id} className="btn btn-primary" onClick={handleEditAddres}>ویرایش</button></div></li>
        </>

    }) : <li>No Address</li>
    return (
        <>
        <h1>لیست آدرس ها</h1>
        <ul className="list-group">
           {addressList}
        </ul>
        
        </>
    )
}
