import React,{useEffect,useState} from 'react'
import CustomInput from '../../CustomInput'
import CustomSelect from '../../CustomSelect'
import useForm from './useForm'
import validate from './addresFormValidator';
import {getprovince,getcity,getCityStates} from '../../../services/cityData'
import {updateById,getItemById} from '../../../services/localStorage'

export default function EditAddres({id}) {
    const { fields, submitHandler, onChangeHandler, errors,changeFields,onBlurHandler } = useForm({
        name: null,
        family: null,
        phone: null,
        province: null,
        city: null,
        has_state: null,
        citystate: null
    },validate)
    const [province, setProvince] = useState([])
    const [city, setCity] = useState([])
    const [citystate, setCityState] = useState([])
    useEffect(() => {
        
        if (province.length === 0) {
            const province =getprovince()
            setProvince(province)
        }
        if (fields.province !== null) {
            const city = getcity(fields.province)
            setCity(city)
            changeFields({...fields,citystate:0,city:0,has_state:0}) 
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [fields.province])
    useEffect(() => {
            const state = getCityStates(fields.city)
            if (state.length === 0) {
                setCityState(state)
                changeFields({...fields,citystate:0,has_state:0}) 
            }else{
                setCityState(state)
                changeFields({...fields,citystate:0,has_state:1}) 
            }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [fields.city])
    useEffect(() => {
        const address = getItemById(id)
        changeFields({name: address.name,
        family: address.family,
        phone: address.phone,
        province: address.province,
        city: address.city,
        has_state: address.has_state,
        citystate: address.citystate})
    }, [id])
    function editAddressHandler(e){
        const formData = submitHandler(e)
        if (formData) {
            updateById(formData,id)
            window.location.replace("/");
        }else{
            return null
        }
    }
    return (
        <form onSubmit={editAddressHandler} noValidate>
                        <CustomInput type="text" onChangeHandler={onChangeHandler} minLength={5}  onBlurHandler={onBlurHandler} className="form-control mb-2" name='name'    errorMessage={errors.name}      value={fields.name || ""} placeholder="نام" required />
                        <CustomInput type="text" onChangeHandler={onChangeHandler}  onBlurHandler={onBlurHandler} className="form-control mb-2" name='family'  errorMessage={errors.family}    value={fields.family || ""} placeholder="نام خانوادکی"/>
                        <CustomInput type="text" onChangeHandler={onChangeHandler}  onBlurHandler={onBlurHandler} className="form-control mb-2" name='phone' errorMessage={errors.phone}  value={fields.phone || ""} placeholder="تلفن همراه"  />
                       
                        <CustomSelect onChangeHandler={onChangeHandler} onBlurHandler={onBlurHandler} name='province' defaultValue={fields.province || 'لطفا گزینه مورد نظر را انتخاب کنید.'} options={province} errorMessage={errors.province} />
                        <CustomSelect onChangeHandler={onChangeHandler} onBlurHandler={onBlurHandler} name='city' defaultValue={fields.city || 'لطفا گزینه مورد نظر را انتخاب کنید.'} options={city} errorMessage={errors.city} />
                        {citystate.length >0 && <CustomSelect onChangeHandler={onChangeHandler} onBlurHandler={onBlurHandler} name='citystate' defaultValue={fields.citystate || 'لطفا گزینه مورد نظر را انتخاب کنید.'} options={citystate} errorMessage={errors.citystate} />}
                        <button className='btn btn-primary'>ذخیره</button>
                    </form>
    )
}
