import {useState} from 'react'
const messages = {
    valueMissing: () => 'مقدار اجباری می باشد!',
    patternMismatch: () => 'فرمت وارده اشتباه می باشد',
    tooShort: (target) => `حداقل باید ${target.minLength} کاراکتر وارد شود!`
}
export default function useForm(initState,validate) {
    const [fields, setFields] = useState(initState)
    const [errors, setErrors] = useState({})
    const validityKeys = Object.keys(messages)


    function changeFields(updateValue){
        setFields({...fields,...updateValue})
    }

    function submitHandler(e){
        //need handle select
        e.preventDefault();
        const fieldErr = validate(fields)

        setErrors(fieldErr)
        const err = Object.keys(fieldErr)
        if (err.length === 0) {
            return fields
        }
        return false
    }
    function onBlurHandler(e){
        const { name, value,type } = e.target;
        // remove whatever error was there previously
        const { [name]: removedError, ...rest } = errors;
        let error;
        // we can use our custom Validator (addresFormValidator)
        if (type === 'select-one') {
            if (value === 'لطفا گزینه مورد نظر را انتخاب کنید.' && e.target.required) {
                error = {[name] :messages['valueMissing']()}
            }
        }else{
            validityKeys.forEach(key=>{
                if (e.target.validity[key]) {
                    error = {[name] :messages[key](e.target)}
                }  
           }) 
        }
        setErrors({...rest,...error});
    }
    function onChangeHandler(e){
        const { name, value } = e.target;
        // save field values
         setFields({
          ...fields,
          [name]: value,
        });
    }
    return {fields,submitHandler,onChangeHandler,errors,changeFields,onBlurHandler}
}
