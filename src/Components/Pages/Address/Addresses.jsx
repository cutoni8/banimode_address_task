import React,{useEffect,useState} from 'react'
import {getItems,deleteItem} from '../../../services/localStorage'
import AddAddress from './AddAddress'
import AddresList from './AddresList'
import EditAddres from './EditAddres'
export default function Addresses() {
   
    const [useraddress, setuUeraddress] = useState([])
    const [action,setAction] = useState({isAdd:false,isEdit:{value:false,id:null}})
    useEffect(() => {
        setuUeraddress(getItems())
    }, [])
    function AddclickHandler(){
        setAction({isAdd:true,isEdit:{value:false,id:null}})
    }
    function handleDeleteAddres(e){
        const {id} = e.target;
        const newAdress= deleteItem(id)
        setuUeraddress(newAdress)
    }
    function handleEditAddres(e){
        const {id} = e.target;
        setAction({isAdd:false,isEdit:{value:true,id:id}})
    }
    return (
        <div className='container mt-4'>
            <div className="row">
                <div className="col-6">
                {!action.isAdd&&!action.isEdit.value&&
                <>
                <button onClick={AddclickHandler} className='btn btn-primary'>افزودن</button>
                <AddresList handleDeleteAddres={handleDeleteAddres} handleEditAddres={handleEditAddres} useraddress={useraddress} />
                </>
                }

                {action.isAdd&&
                <AddAddress />
                }
                {action.isEdit.value&&
                <EditAddres id={action.isEdit.id} />
                }   
                
                </div>
            </div>
        </div>
    )
}
