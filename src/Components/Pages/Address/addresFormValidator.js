export default function validateInfo(values) {
    let errors = {};
  
    if (!values.name.trim()) {
      errors.name = 'Name required';
    }
    if (!values.family) {
        errors.family = 'Family required';
      } 
    // if (!values.email) {
    //   errors.email = 'Email required';
    // } else if (!/\S+@\S+\.\S+/.test(values.email)) {
    //   errors.email = 'Email address is invalid';
    // }
    if (!values.province) {
      errors.province = 'Province is required';
    } 
    if (!values.city) {
        errors.city = 'City is required';
    }
    if (!values.citystate && values.has_state) {
        errors.citystate = 'Citystate is required';
    }
    return errors;
  }