export default function CustomInput({onChangeHandler,onBlurHandler,errorMessage,...tagAttrs}) {
    return (
    <>
    <input {...tagAttrs}  onBlur={onBlurHandler} onChange={onChangeHandler} />
    {errorMessage &&<span>{errorMessage}</span>}
    </>
    )
}
