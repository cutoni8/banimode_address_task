import React from 'react'

export default function CustomSelect({onChangeHandler,defaultValue,onBlurHandler,errorMessage,options,name}) {
    const selectOptins =options ? options.map((option) => (
        <option key = {option.id} value={option.id} >{option.name}</option>
      )) : null
    return (
       <>
        <select className="form-select"
         required
         name={name}
         aria-label="Default select example"  
         onChange={onChangeHandler}
         onBlur={onBlurHandler}  
         defaultValue={defaultValue}
         >
            <option value={defaultValue}  >{defaultValue}</option>
            {selectOptins}

        </select>
        {errorMessage &&<span>{errorMessage}</span>}
       </>
    )
}
