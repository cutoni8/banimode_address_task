/* eslint-disable eqeqeq */
import Country from './city.json'


export function getprovince(){
    return Country.province;
}

export function getcity(provinceID){
    return provinceID ? Country.city.filter(city=>city.id_province == provinceID) : []
}

export function getCityStates(cityID){
    return cityID ? Country.state.filter(state=>state.id_city == cityID) : []
}