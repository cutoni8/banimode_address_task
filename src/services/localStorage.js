import { v4 as uuidv4 } from 'uuid';

const key = 'addresses'

export function setItem(data){
    let addresses = getItems()
    if (addresses == null) addresses = [];
    data.id = uuidv4();
    addresses.push(data);
    window.localStorage.setItem(key, JSON.stringify(addresses));
    
}
export function updateById(data,id) {
    deleteItem(id)
    setItem(data)
}
export function getItems(){
    return JSON.parse(localStorage.getItem(key))
}

export function getItemById(id){
    let allAddress = getItems()
    const address = allAddress.map(address=>{
        return address.id === id ? address : false
    }).filter(Boolean)
    return address[0];
}

export function update(data){
    window.localStorage.setItem(key, JSON.stringify(data));
}


export function deleteItem(id){
    let allAddress = getItems()
    const newUSerAddress = allAddress.map(address=>{
        return address.id === id ? false : address
    }).filter(Boolean)
    
    update(newUSerAddress)
    return newUSerAddress;
}